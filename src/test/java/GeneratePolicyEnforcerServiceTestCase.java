import org.junit.jupiter.api.Test;
import xtool.plugins.model.PolicyEnforcer;
import xtool.plugins.service.GeneratePolicyEnforcerService;
import xtool.plugins.service.YmlService;

import java.io.File;
import java.util.List;

public class GeneratePolicyEnforcerServiceTestCase {

    @Test
    public void genTest() {
        List<PolicyEnforcer> resourcePermissionDefs = YmlService.readPolicyEnforcers(new File("src/test/resources/keycloak"));
        GeneratePolicyEnforcerService generatePolicyEnforcerService = new GeneratePolicyEnforcerService();
        byte[] generate = generatePolicyEnforcerService.generate(resourcePermissionDefs);
        System.out.println(new String(generate));
    }
}
