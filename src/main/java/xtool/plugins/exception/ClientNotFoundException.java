package xtool.plugins.exception;


public class ClientNotFoundException extends RuntimeException {
    public ClientNotFoundException(String clientId) {
        super("Client não encontrado. ClientId=" + clientId);
    }
}
