package xtool.plugins.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@ToString
public class ProtocolMapperDef {
    private String name;
    private String protocol;
    private String type;
    private Map<String, String> config = new HashMap<>();
}
