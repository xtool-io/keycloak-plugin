package xtool.plugins.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ScopeMappingDef {
    private String client;
    private boolean allRoles = false;
    private List<String> roles = new ArrayList<>();
}
