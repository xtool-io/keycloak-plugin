package xtool.plugins.model;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.text.StringSubstitutor;
import org.keycloak.representations.idm.ClientRepresentation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@SuppressWarnings("OptionalUsedAsFieldOrParameterType")
@ToString
@Getter
@Setter
@JsonTypeName("client")
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
public class ClientDef {
    protected String clientId;
    protected String name;
    protected String description;
    protected String rootUrl;
    protected String adminUrl;
    protected String baseUrl;
    protected boolean surrogateAuthRequired;
    protected boolean enabled = true;
    protected boolean alwaysDisplayInConsole;
    protected String clientAuthenticatorType;
    protected String secret;
    protected String registrationAccessToken;
    //    protected String[] defaultRoles;
    protected List<String> redirectUris;
    protected List<String> webOrigins;
    protected Integer notBefore;
    protected boolean bearerOnly;
    protected boolean consentRequired;
    protected boolean standardFlowEnabled;
    protected boolean implicitFlowEnabled;
    protected boolean directAccessGrantsEnabled;
    protected boolean serviceAccountsEnabled;
    //    protected Boolean oauth2DeviceAuthorizationGrantEnabled;
    protected boolean authorizationServicesEnabled;
    protected boolean directGrantsOnly;
    protected boolean publicClient;
    protected boolean frontchannelLogout;
    protected String protocol;
    protected Map<String, String> attributes;
    protected Map<String, String> authenticationFlowBindingOverrides;
    protected boolean fullScopeAllowed = false;
    protected Integer nodeReRegistrationTimeout;
    protected Map<String, Integer> registeredNodes;
//    protected String clientTemplate;
//    protected List<String> defaultClientScopes;
//    protected List<String> optionalClientScopes;
//    protected String origin;
    /**
     * Definição das Roles
     */
    private List<RoleDef> roles = new ArrayList<>();

    /**
     * Definição das Composite Roles
     */
    private List<CompositeRoleDef> compositeRoles = new ArrayList<>();
    /**
     * Definição do Scope Mappings
     */
    private List<ScopeMappingDef> scopeMappings = new ArrayList<>();

    /**
     * Definições de Autorização.
     */
//    private Optional<AuthorizationDef> authorization = Optional.empty();

    /**
     * Definição de Protocol Mappers
     */
    private List<ProtocolMapperDef> protocolMappers = new ArrayList<>();

    public static ClientRepresentation of(ClientDef clientDef, StringSubstitutor substitutor) {
        ClientRepresentation representation = new ClientRepresentation();
        representation.setClientId(clientDef.getClientId());
        representation.setName(clientDef.getName());
        representation.setDescription(clientDef.getDescription());
        representation.setRootUrl(clientDef.getRootUrl());
        representation.setAdminUrl(clientDef.getAdminUrl());
        representation.setBaseUrl(clientDef.getBaseUrl());
        representation.setSurrogateAuthRequired(clientDef.isSurrogateAuthRequired());
        representation.setEnabled(clientDef.isEnabled());
        representation.setAlwaysDisplayInConsole(clientDef.isAlwaysDisplayInConsole());
        representation.setClientAuthenticatorType(clientDef.getClientAuthenticatorType());
        representation.setSecret(substitutor.replace(clientDef.getSecret()));
        representation.setRegistrationAccessToken(clientDef.getRegistrationAccessToken());
        representation.setRedirectUris(clientDef.getRedirectUris());
        representation.setWebOrigins(clientDef.getWebOrigins());
        representation.setNotBefore(clientDef.getNotBefore());
        representation.setBearerOnly(clientDef.isBearerOnly());
        representation.setConsentRequired(clientDef.isConsentRequired());
        representation.setStandardFlowEnabled(clientDef.isStandardFlowEnabled());
        representation.setImplicitFlowEnabled(clientDef.isImplicitFlowEnabled());
        representation.setDirectAccessGrantsEnabled(clientDef.isDirectAccessGrantsEnabled());
        representation.setServiceAccountsEnabled(clientDef.isServiceAccountsEnabled());
        representation.setAuthorizationServicesEnabled(clientDef.isAuthorizationServicesEnabled());
        representation.setDirectGrantsOnly(clientDef.isDirectGrantsOnly());
        representation.setPublicClient(clientDef.isPublicClient());
        representation.setFrontchannelLogout(clientDef.isFrontchannelLogout());
        representation.setProtocol(clientDef.getProtocol());
        representation.setAttributes(clientDef.getAttributes());
        representation.setAuthenticationFlowBindingOverrides(clientDef.getAuthenticationFlowBindingOverrides());
        representation.setFullScopeAllowed(clientDef.isFullScopeAllowed());
        representation.setNodeReRegistrationTimeout(clientDef.getNodeReRegistrationTimeout());
        return representation;
    }

}
