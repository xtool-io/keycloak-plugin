package xtool.plugins.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLParser;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import xtool.plugins.model.ClientDef;
import xtool.plugins.model.PolicyEnforcer;
import xtool.plugins.model.ResourceDef;
import xtool.plugins.model.permission.ResourcePermissionDef;
import xtool.plugins.model.permission.ScopePermissionDef;
import xtool.plugins.model.policy.JsPolicyDef;
import xtool.plugins.model.policy.RolePolicyDef;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Classe com as funções de leitura e validação do arquivo descritor
 */
@Slf4j
public class YmlService {

    private YmlService() {
    }


    @SneakyThrows
    private static <T> T read(Class<T> clasType, File file) {
        log.info("--- {}", file);
        ObjectMapper om = new ObjectMapper(new YAMLFactory());
        om.registerModule(new Jdk8Module());
        om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
//        om.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
        return om.readValue(file, clasType);
    }

    @SneakyThrows
    private static <T> List<T> readAll(Class<T> classType, File file) {
        log.info("--- {}", file);
        YAMLFactory yamlFactory = new YAMLFactory();
        YAMLParser yamlParser = yamlFactory.createParser(file);
        ObjectMapper om = new ObjectMapper(yamlFactory);
        om.registerModule(new Jdk8Module());
        om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
        om.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
        return om.readValues(yamlParser, classType).readAll();
    }

    @SneakyThrows
    public static List<ClientDef> readClients(File file) {
        return Files.walk(Paths.get(file.getAbsolutePath()))
            .filter(path -> path.toFile().getAbsolutePath().endsWith(".client.yml"))
            .map(path -> read(ClientDef.class, path.toFile()))
            .collect(Collectors.toList());
    }

    @SneakyThrows
    public static List<ResourceDef> readResources(File file) {
        return Files.walk(Paths.get(file.getAbsolutePath()))
            .filter(path -> path.toFile().getAbsolutePath().endsWith(".resource.yml"))
            .map(path -> readAll(ResourceDef.class, path.toFile()))
            .flatMap(Collection::stream)
            .collect(Collectors.toList());
    }

    @SneakyThrows
    public static List<RolePolicyDef> readRolePolicies(File file) {
        return Files.walk(Paths.get(file.getAbsolutePath()))
            .filter(path -> path.toFile().getAbsolutePath().endsWith(".role.policy.yml"))
            .map(path -> read(RolePolicyDef.class, path.toFile()))
            .collect(Collectors.toList());
    }

    @SneakyThrows
    public static List<JsPolicyDef> readJsPolicies(File file) {
        return Files.walk(Paths.get(file.getAbsolutePath()))
            .filter(path -> path.toFile().getAbsolutePath().endsWith(".js.policy.yml"))
            .map(path -> read(JsPolicyDef.class, path.toFile()))
            .collect(Collectors.toList());
    }

    @SneakyThrows
    public static List<ScopePermissionDef> readScopePermissions(File file) {
        return Files.walk(Paths.get(file.getAbsolutePath()))
            .filter(path -> path.toFile().getAbsolutePath().endsWith(".scope.permission.yml"))
            .map(path -> readAll(ScopePermissionDef.class, path.toFile()))
            .flatMap(Collection::stream)
            .collect(Collectors.toList());
    }

    @SneakyThrows
    public static List<ResourcePermissionDef> readResourcePermissions(File file) {
        return Files.walk(Paths.get(file.getAbsolutePath()))
            .filter(path -> path.toFile().getAbsolutePath().endsWith(".resource.permission.yml"))
            .map(path -> readAll(ResourcePermissionDef.class, path.toFile()))
            .flatMap(Collection::stream)
            .collect(Collectors.toList());
    }

    @SneakyThrows
    public static List<PolicyEnforcer> readPolicyEnforcers(File file) {
        return Files.walk(Paths.get(file.getAbsolutePath()))
            .filter(path -> path.toFile().getAbsolutePath().endsWith(".policy.enforcer.yml"))
            .map(path -> readAll(PolicyEnforcer.class, path.toFile()))
            .flatMap(Collection::stream)
            .collect(Collectors.toList());
    }

}
