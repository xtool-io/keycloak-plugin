package xtool.plugins.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;
import org.keycloak.admin.client.resource.ClientResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.ProtocolMapperRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.authorization.*;
import xtool.plugins.model.*;
import xtool.plugins.model.permission.ResourcePermissionDef;
import xtool.plugins.model.permission.ScopePermissionDef;
import xtool.plugins.model.policy.JsPolicyDef;
import xtool.plugins.model.policy.RolePolicyDef;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
public class ClientDefService {

    private final KeycloakService keycloakService;

    private final StringSubstitutor substitutor;

    public ClientDefService(KeycloakService keycloakService, StringSubstitutor substitutor) {
        this.keycloakService = keycloakService;
        this.substitutor = substitutor;
    }

    public void registerRole(ClientDef clientDef) {
        if (StringUtils.isBlank(clientDef.getClientId())) clientDef.setClientId(clientDef.getName());
        log.info(".ClientId: {}", clientDef.getName());
        log.info(" └ Description: {}", clientDef.getDescription());
        log.info(" └ RootUrl: {}", clientDef.getRootUrl());
        log.info(" └ AdminUrl: {}", clientDef.getAdminUrl());
        log.info(" └ BaseUrl: {}", clientDef.getBaseUrl());
        log.info(" └ RedirectUris: {}", clientDef.getRedirectUris());
        log.info(" └ PublicClient: {}", clientDef.isPublicClient());
        log.info(" └ BearerOnly: {}", clientDef.isBearerOnly());
        log.info(" └ ConsentRequired: {}", clientDef.isConsentRequired());
        log.info(" └ StandardFlowEnabled: {}", clientDef.isStandardFlowEnabled());
        log.info(" └ ImplicitFlowEnabled: {}", clientDef.isImplicitFlowEnabled());
        log.info(" └ DirectAccessGrantsEnabled: {}", clientDef.isDirectAccessGrantsEnabled());
        log.info(" └ ServiceAccountsEnabled: {}", clientDef.isServiceAccountsEnabled());
        log.info(" └ AuthorizationServicesEnabled: {}", clientDef.isAuthorizationServicesEnabled());
        log.info(" └ DirectGrantsOnly: {}", clientDef.isDirectGrantsOnly());
        ClientRepresentation representation = ClientDef.of(clientDef, substitutor);
        this.keycloakService.updateClient(representation);
        clientDef.getRoles().forEach(roleDef -> registerRole(representation, roleDef));
        clientDef.getCompositeRoles().forEach(compositeRoleDef -> registerCompositeRole(representation, compositeRoleDef));
        clientDef.getProtocolMappers().forEach(protocolMapperDef -> updateProtocolMapper(representation, protocolMapperDef));
        this.removeDefaultResource(representation);
        this.removeDefaultPolicy(representation);
    }

    private void removeDefaultPolicy(ClientRepresentation clientRepresentation) {
        if (clientRepresentation.getAuthorizationServicesEnabled()) {
            keycloakService.removePolicy(clientRepresentation, "Default Policy");
            log.info("..(-) Policy: Default Policy");
        }
    }

    private void removeDefaultResource(ClientRepresentation clientRepresentation) {
        if (clientRepresentation.getAuthorizationServicesEnabled()) {
            keycloakService.removeResource(clientRepresentation, "Default Resource");
            log.info("..(-) Resource: Default Resource");
        }
    }

    private void registerRole(ClientRepresentation clientRepresentation, RoleDef roleDef) {
        if (Objects.nonNull(roleDef)) {
            RoleRepresentation roleRepresentation = new RoleRepresentation();
            roleRepresentation.setName(roleDef.getName());
            roleRepresentation.setDescription(roleDef.getDescription());
            keycloakService.createRole(clientRepresentation, roleRepresentation);
            log.info("..(+) Role: {}", roleRepresentation.getName());
            log.info("      └ Description: {}", roleDef.getDescription());
        }
    }

    private void registerCompositeRole(ClientRepresentation clientRepresentation, CompositeRoleDef compositeRoleDef) {
        if (Objects.nonNull(compositeRoleDef)) {
            keycloakService.registerCompositeRole(
                    clientRepresentation,
                    compositeRoleDef.getRole(),
                    compositeRoleDef.getComposites()
            );
            log.info("..(~) Role: {}", compositeRoleDef.getRole());
            log.info("      └ Composites: {}", compositeRoleDef.getComposites());
        }
    }


    private void updateProtocolMapper(ClientRepresentation clientRepresentation, ProtocolMapperDef protocolMapperDef) {
        ProtocolMapperRepresentation protocolMapperRepresentation = new ProtocolMapperRepresentation();
        protocolMapperRepresentation.setProtocolMapper(protocolMapperDef.getType());
        protocolMapperRepresentation.setProtocol(protocolMapperDef.getProtocol());
        protocolMapperRepresentation.setConfig(protocolMapperDef.getConfig());
        protocolMapperRepresentation.setName(protocolMapperDef.getName());
        keycloakService.updateProtocolMapper(clientRepresentation, protocolMapperRepresentation);
        log.info("..(+) Protocol Mapper: {}", protocolMapperDef.getName());
        log.info("      └ Type: {} ", protocolMapperRepresentation.getProtocolMapper());
        log.info("      └ Protocol: {} ", protocolMapperRepresentation.getProtocol());
        log.info("      └ Config: {} ", protocolMapperRepresentation.getConfig());
    }

    public void updateScopeMapping(String clientId1, List<ScopeMappingDef> scopeMappingsDef) {
        for (ScopeMappingDef scopeMappingDef : scopeMappingsDef) {
            if (scopeMappingDef.isAllRoles()) {
                List<String> roles = keycloakService.findAllRoles(scopeMappingDef.getClient()).stream()
                        .map(RoleRepresentation::getName)
                        .collect(Collectors.toList());
                keycloakService.updateScopeMapping(
                        clientId1,
                        scopeMappingDef.getClient(),
                        roles
                );
                log.info("..(+) Scope Mapping Client: {}", scopeMappingDef.getClient());
                log.info("      └ AllRoles: {} ", scopeMappingDef.isAllRoles());
                log.info("      └ Roles: {} ", scopeMappingDef.getRoles());
                return;
            }
            keycloakService.updateScopeMapping(clientId1, scopeMappingDef.getClient(), scopeMappingDef.getRoles());
            log.info("..(+) Scope Mapping Client: {}", scopeMappingDef.getClient());
            log.info("      └ AllRoles: {} ", scopeMappingDef.isAllRoles());
            log.info("      └ Roles: {} ", scopeMappingDef.getRoles());
        }
    }

    public void registerRole(ResourceDef resourceDef) {
        ClientResource clientResource = keycloakService.getClientResourceByClientId(resourceDef.getClientId());
        ResourceRepresentation resourceRepresentation = new ResourceRepresentation();
        resourceRepresentation.setName(resourceDef.getName());
        resourceRepresentation.setScopes(
                resourceDef.getScopes().stream()
                        .map(ScopeRepresentation::new)
                        .collect(Collectors.toSet())
        );
        resourceRepresentation.setUris(resourceDef.getUris());
        resourceRepresentation.setType(resourceDef.getType());
        resourceRepresentation.setAttributes(resourceDef.getAttributes());
        keycloakService.updateResource(clientResource.toRepresentation(), resourceRepresentation);
        log.info("..(+) Resource: {}", resourceRepresentation.getName());
        log.info("      └ Uris: {}", resourceDef.getUris());
        log.info("      └ Scopes: {}", resourceDef.getScopes());
        log.info("      └ Type: {}", resourceDef.getType());
        log.info("      └ Attributes: {}", resourceDef.getAttributes());
    }

    public void registerRole(RolePolicyDef rolePolicyDef) {
        ClientResource clientResource = keycloakService.getClientResourceByClientId(rolePolicyDef.getClientId());
        RolePolicyRepresentation rolePolicyRepresentation = new RolePolicyRepresentation();
        rolePolicyRepresentation.setName(rolePolicyDef.getName());
        rolePolicyRepresentation.setDescription(rolePolicyDef.getDescription());
        rolePolicyDef.getClient().ifPresent(clientDef -> {
            clientDef.getRoles().forEach(role -> rolePolicyRepresentation.addClientRole(
                    clientDef.getName(),
                    role)
            );
        });
        keycloakService.updatePolicy(clientResource.toRepresentation(), rolePolicyRepresentation);
        log.info("..(+) Policy: {}", rolePolicyDef.getName());
        log.info("      └ Type: Role");
        log.info("      └ Description: {}", rolePolicyDef.getDescription());
        rolePolicyDef.getRealm().ifPresent(realmDef -> log.info("      └ Realm Roles: {}", realmDef.getRoles()));
        rolePolicyDef.getClient().ifPresent(clientDef -> log.info("      └ Client Roles: {}", clientDef.getRoles()));

    }

    public void registerRole(JsPolicyDef jsPolicyDef) {
        ClientResource clientResource = keycloakService.getClientResourceByClientId(jsPolicyDef.getClientId());
        JSPolicyRepresentation jsPolicyRepresentation = new JSPolicyRepresentation();
        jsPolicyRepresentation.setName(jsPolicyDef.getName());
        jsPolicyRepresentation.setDescription(jsPolicyDef.getDescription());
        jsPolicyRepresentation.setCode(jsPolicyDef.getCode());
        keycloakService.updatePolicy(clientResource.toRepresentation(), jsPolicyRepresentation);
        log.info("..(+) Policy: {}", jsPolicyDef.getName());
        log.info("      └ Type: Js");
        log.info("      └ Description: {}", jsPolicyDef.getDescription());
        log.info("      └ Code: {}", jsPolicyDef.getCode());
    }

    public void registerRole(ScopePermissionDef scopePermissionDef) {
        ClientResource clientResource = keycloakService.getClientResourceByClientId(scopePermissionDef.getClientId());
        ScopePermissionRepresentation permissionRepresentation = new ScopePermissionRepresentation();
        permissionRepresentation.setName(scopePermissionDef.getName());
        permissionRepresentation.setDescription(scopePermissionDef.getDescription());
        permissionRepresentation.setDecisionStrategy(scopePermissionDef.getDecisionStrategy());
        permissionRepresentation.setPolicies(scopePermissionDef.getPolicies());
        permissionRepresentation.setScopes(scopePermissionDef.getScopes());
        permissionRepresentation.setResources(scopePermissionDef.getResources());
        keycloakService.updatePermission(clientResource.toRepresentation(), permissionRepresentation);
        log.info("..(+) Permission: {}", scopePermissionDef.getName());
        log.info("      └ Type: Scope ");
        log.info("      └ Resources: {}", permissionRepresentation.getResources());
        log.info("      └ Scopes: {}", permissionRepresentation.getScopes());
        log.info("      └ Policies: {}", permissionRepresentation.getPolicies());
    }

    public void registerRole(ResourcePermissionDef resourcePermissionDef) {
        ClientResource clientResource = keycloakService.getClientResourceByClientId(resourcePermissionDef.getClientId());
        ResourcePermissionRepresentation permissionRepresentation = new ResourcePermissionRepresentation();
        permissionRepresentation.setName(resourcePermissionDef.getName());
        permissionRepresentation.setDescription(resourcePermissionDef.getDescription());
        permissionRepresentation.setDecisionStrategy(resourcePermissionDef.getDecisionStrategy());
        permissionRepresentation.setResources(resourcePermissionDef.getResources());
        permissionRepresentation.setPolicies(resourcePermissionDef.getPolicies());
        if (resourcePermissionDef.isApplyToResourceType()) {
            permissionRepresentation.setResourceType(resourcePermissionDef.getResourceType());
        }
        keycloakService.updatePermission(clientResource.toRepresentation(), permissionRepresentation);
        log.info("..(+) Permission: {}", resourcePermissionDef.getName());
        log.info("      └ Type: Resource ");
        log.info("      └ Resources: {}", permissionRepresentation.getResources());
        log.info("      └ Policies: {}", permissionRepresentation.getPolicies());

    }


}
