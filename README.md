# keycloak-plugin

Plugin maven com a finalidade de gerenciar o registro de `clients` no keycloak. As definições para registro
são definidas em arquivos *.yml localizados em `src/main/resources/keycloak`.

# Instalação

```xml
<build>
    <plugins>
        <plugin>
            <groupId>xtool.plugins</groupId>
            <artifactId>keycloak-plugin</artifactId>
            <version>1.0.0</version>
            <configuration>
                <user>${env.KEYCLOAK_CLIENT_CREATOR_USERNAME}</user>
                <password>${env.KEYCLOAK_CLIENT_CREATOR_PASSWORD}</password>
                <url>https://dev-k8s.tre-pa.jus.br/auth</url>
                <realm>XYZ</realm>
                <properties>
                    <keycloak.client.secret>${env.KEYCLOAK_CREDENTIALS_SECRET}</keycloak.client.secret>
                </properties>
            </configuration>
        </plugin>
    </plugins>
</build>
```

Para realizar o registro no keycloak é necessário a definição de um usuário e senha (tag `<user>` e `<password>` em 
`<configuration>`) com as seguintes definicões de `Role Mapping`: 

- No client `realm-management` adcionar as roles: `manage-clients` e `manage-authorization`


# Estrutura do arquivo .yml

````yml
---
keycloak:
  client:
    name:                                # Nome do client
    secret: ${keycloak.client.secret}    # Secret do client (Passado via 
    baseUrl: 
    roles:                               # Lista de roles do client
      - name:                            # Nome da role
        description:                     # Descrição da role
    authorization:                       # Definições de autorização
      resources:                         # Definiões dos recursos
        - name:                          # 
          uris: []
          scopes: []
      policies:                          # Lista com as definições de policies
        - !<role.policy>                 # Definição de Policy do tipo Role
          name:                          # Nome da Role
          description:                   # Descrição da Role
          client:                        
            name:                        # Nome do client 
            roles: []                    # Roles do client
      permissions:                       # Lista com as definições de permissions
        - !<scope.permission>            # Definição de Permission do tipo Scope 
          name:                          # Nome da Permission do tipo Scope
          resources: []                  # Lista com os nomes dos recursos
          scopes: []                     # Lista com os nomes dos Scopes
          policies: []                   # Listam com os nomes das policies
        - !<resource.permission>         # Definição de Permission do tipo Resource
          name:                          # Nome da permission do tipo Resource
          description:                   # Descrição da permission
          resources: []                  # Lista com os nomes dos recursos
          policies: []                   # 
    protocolMappers:
        - name: 
          type: 
          protocol: 
          config:
            claim.name: 
            full.path: 
            id.token.claim: 
            access.token.claim: 
            userinfo.token.claim: 
    scopeMappings:
        - client: 
          roles: []
        - client: 
          allRoles: true
````

