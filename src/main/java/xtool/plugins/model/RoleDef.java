package xtool.plugins.model;

import lombok.Data;

@Data
public class RoleDef {
    private String name;
    private String description;
}
