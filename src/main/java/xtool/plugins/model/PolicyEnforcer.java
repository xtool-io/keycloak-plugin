package xtool.plugins.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.keycloak.representations.adapters.config.PolicyEnforcerConfig;

@Getter
@Setter
//@JsonTypeName("keycloak")
//@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
public class PolicyEnforcer {
    @JsonProperty("policy-enforcer-config")
    private PolicyEnforcerConfig policyEnforcerConfig;
}
