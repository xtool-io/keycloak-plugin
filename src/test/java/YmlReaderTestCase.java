import org.junit.jupiter.api.Test;
import xtool.plugins.model.*;
import xtool.plugins.model.permission.ScopePermissionDef;
import xtool.plugins.model.policy.JsPolicyDef;
import xtool.plugins.model.policy.RolePolicyDef;
import xtool.plugins.service.YmlService;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class YmlReaderTestCase {

    @Test
    public void readClients() throws IOException {
        List<ClientDef> clientDefs = YmlService.readClients(new File("src/test/resources/keycloak"));
        clientDefs.stream()
            .forEach(clientDef -> System.out.println(clientDef.getName()));
    }

    @Test
    public void readResources() throws IOException {
        List<ResourceDef> resourceDefs = YmlService.readResources(new File("src/test/resources/keycloak"));
        resourceDefs.stream()
            .forEach(resourceDef -> System.out.println(resourceDef.getName()));
    }

    @Test
    public void readRolePolicies() throws IOException {
        List<RolePolicyDef> rolePolicyDefs = YmlService.readRolePolicies(new File("src/test/resources/keycloak"));
        rolePolicyDefs.stream()
            .forEach(rolePolicyDef -> System.out.println(rolePolicyDef.getName()));
    }

    @Test
    public void readJsPolicies() throws IOException {
        List<JsPolicyDef> jsPolicyDefs = YmlService.readJsPolicies(new File("src/test/resources/keycloak"));
        jsPolicyDefs.stream()
            .forEach(jsPolicyDef -> System.out.println(jsPolicyDef.getName()));
    }


    @Test
    public void readScopePermissions() throws IOException {
        List<ScopePermissionDef> scopePermissionDefs = YmlService.readScopePermissions(new File("src/test/resources/keycloak"));
        scopePermissionDefs.stream()
            .forEach(scopePermissionDef -> System.out.println(scopePermissionDef.getName()));
    }

    @Test
    public void readResourcePermissions() throws IOException {
        List<PolicyEnforcer> resourcePermissionDefs = YmlService.readPolicyEnforcers(new File("src/test/resources/keycloak"));
        resourcePermissionDefs.stream()
            .forEach(resourcePermissionDef -> System.out.println(resourcePermissionDef.getPolicyEnforcerConfig().getPaths()));
    }

}
