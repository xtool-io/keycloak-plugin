package xtool.plugins.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CompositeRoleDef {
    private String role;

    private List<String> composites = new ArrayList<>();
}
