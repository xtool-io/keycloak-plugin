package xtool.plugins.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Getter;
import lombok.Setter;

import java.util.*;

@Getter
@Setter
@JsonTypeName("resource")
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
public class ResourceDef {
    private String clientId;
    private String name;
    private String type;
    private Set<String> uris = new HashSet<>();
    private List<String> scopes = new ArrayList<>();
    private Map<String, List<String>> attributes = new HashMap<>();
}
