package xtool.plugins.model.permission;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Getter;
import lombok.Setter;
import org.keycloak.representations.idm.authorization.DecisionStrategy;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@JsonTypeName("scopePermission")
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
public class ScopePermissionDef  {
    private String clientId;
    private String name;
    private String description;
    private DecisionStrategy decisionStrategy = DecisionStrategy.UNANIMOUS;
    private Set<String> resources = new HashSet<>();
    private Set<String> scopes = new HashSet<>();
    private Set<String> policies = new HashSet<>();
}
