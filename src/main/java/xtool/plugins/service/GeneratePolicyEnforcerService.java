package xtool.plugins.service;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import lombok.SneakyThrows;
import org.keycloak.representations.adapters.config.PolicyEnforcerConfig;
import xtool.plugins.model.PolicyEnforcer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GeneratePolicyEnforcerService {

    @SneakyThrows
    public byte[] generate(List<PolicyEnforcer> policyEnforcers) {
        ObjectMapper om = new ObjectMapper(new YAMLFactory());
        om.registerModule(new Jdk8Module());
//        om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
//        om.configure(SerializationFeature.WRAP_ROOT_VALUE, true);
        om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        PolicyEnforcer policyEnforcer = new PolicyEnforcer();
        PolicyEnforcerConfig policyEnforcerConfig = new PolicyEnforcerConfig();
        policyEnforcerConfig.setEnforcementMode(null);
        policyEnforcers.stream()
            .map(pe -> pe.getPolicyEnforcerConfig().getPaths())
            .forEach(pathConfigs -> policyEnforcerConfig.getPaths().addAll(pathConfigs));
        policyEnforcer.setPolicyEnforcerConfig(policyEnforcerConfig);
        Map<String, Object> keycloakWrapper = new HashMap<>();
        keycloakWrapper.put("keycloak", policyEnforcer);
        return om.writerWithDefaultPrettyPrinter()
            .writeValueAsBytes(keycloakWrapper);
    }
}
