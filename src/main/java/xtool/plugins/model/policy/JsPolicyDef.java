package xtool.plugins.model.policy;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Getter;
import lombok.Setter;
import org.keycloak.representations.idm.authorization.Logic;

@Getter
@Setter
@JsonTypeName("jsPolicy")
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
public class JsPolicyDef  {
    private String clientId;
    private String name;
    private String description;
    private Logic logic = Logic.POSITIVE;
    private String code;
}
