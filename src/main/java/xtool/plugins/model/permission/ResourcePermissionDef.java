package xtool.plugins.model.permission;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Getter;
import lombok.Setter;
import org.keycloak.representations.idm.authorization.DecisionStrategy;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@JsonTypeName("resourcePermission")
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
public class ResourcePermissionDef {
    private String clientId;
    private String name;
    private String description;
    private DecisionStrategy decisionStrategy = DecisionStrategy.UNANIMOUS;
    private boolean applyToResourceType = false;
    private String resourceType;
    private Set<String> resources = new HashSet<>();
    private Set<String> policies = new HashSet<>();
}
