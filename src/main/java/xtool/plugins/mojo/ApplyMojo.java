package xtool.plugins.mojo;

import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.text.StringSubstitutor;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import xtool.plugins.model.ClientDef;
import xtool.plugins.model.ResourceDef;
import xtool.plugins.model.permission.ResourcePermissionDef;
import xtool.plugins.model.permission.ScopePermissionDef;
import xtool.plugins.model.policy.JsPolicyDef;
import xtool.plugins.model.policy.RolePolicyDef;
import xtool.plugins.service.ClientDefService;
import xtool.plugins.service.KeycloakService;
import xtool.plugins.service.YmlService;

import java.io.File;
import java.util.List;
import java.util.Properties;

/**
 * Mojo que realiza a leitura do arquivo descritor *.yml e executa as operações de registros de clients, roles,
 * e informações de autorização no Keycloak.
 */
@Slf4j
@Mojo(name = "apply")
public class ApplyMojo extends AbstractMojo {

    /**
     * Parâmetro com a refência ao arquivo descritor das informações do Keycloak. (keycloak.yml)
     */
    @Parameter(property = "keycloak.descriptor", defaultValue = "src/main/resources/keycloak")
    private File sourceDirectory;

    /**
     * Usuário com permissão de gerenciamento de clientes no realm correspondente.
     * realm-management: [manage-clients, manage-authorization]
     */
    @Parameter(property = "keycloak.user", required = true, defaultValue = "keycloak_client_creator_user")
    private String user;

    /**
     * Senha do usuário.
     */
    @Parameter(property = "keycloak.password", required = true, defaultValue = "password")
    private String password;

    /**
     * URL de conexão do Keycloak
     */
    @Parameter(property = "keycloak.url", required = true, defaultValue = "http://localhost:8085/auth")
    private String url;

    /**
     * Nome do REALM de serviço
     */
    @Parameter(property = "keycloak.realm", required = true)
    private String realm;

    @Parameter
    private Properties properties;

    public void execute() throws MojoExecutionException, MojoFailureException {
        log.info("URL: {}", url);
        log.info("Realm: {}", realm);
        log.info("Source Directory: {}", sourceDirectory);
        this.processClients();
        this.processResources();
        this.processRolePolicies();
        this.processJsPolicies();
        this.processResourcePermissions();
        this.processScopePermissions();

    }

    private void processClients() {
        log.info("===> Iniciando processamento dos Clients...");
        KeycloakService keycloakService = KeycloakService.from(url, user, password, realm);
        StringSubstitutor substitutor = new StringSubstitutor(Maps.newHashMap(Maps.fromProperties(properties)));
        ClientDefService service = new ClientDefService(keycloakService, substitutor);
        List<ClientDef> clientDefs = YmlService.readClients(sourceDirectory);
        clientDefs.forEach(service::registerRole);
        clientDefs.forEach(clientDef -> service.updateScopeMapping(clientDef.getClientId(), clientDef.getScopeMappings()));
    }

    private void processResources() {
        log.info("===> Iniciando processamento dos Resources...");
        KeycloakService keycloakService = KeycloakService.from(url, user, password, realm);
        StringSubstitutor substitutor = new StringSubstitutor(Maps.newHashMap(Maps.fromProperties(properties)));
        ClientDefService service = new ClientDefService(keycloakService, substitutor);
        List<ResourceDef> resourceDefs = YmlService.readResources(sourceDirectory);
        resourceDefs.forEach(service::registerRole);
    }

    private void processRolePolicies() {
        log.info("===> Iniciando processamento das Role Policies...");
        KeycloakService keycloakService = KeycloakService.from(url, user, password, realm);
        StringSubstitutor substitutor = new StringSubstitutor(Maps.newHashMap(Maps.fromProperties(properties)));
        ClientDefService service = new ClientDefService(keycloakService, substitutor);
        List<RolePolicyDef> rolePolicyDefs = YmlService.readRolePolicies(sourceDirectory);
        rolePolicyDefs.forEach(service::registerRole);
    }

    private void processJsPolicies() {
        log.info("===> Iniciando processamento das JS Policies...");
        KeycloakService keycloakService = KeycloakService.from(url, user, password, realm);
        StringSubstitutor substitutor = new StringSubstitutor(Maps.newHashMap(Maps.fromProperties(properties)));
        ClientDefService service = new ClientDefService(keycloakService, substitutor);
        List<JsPolicyDef> rolePolicyDefs = YmlService.readJsPolicies(sourceDirectory);
        rolePolicyDefs.forEach(service::registerRole);
    }

    private void processResourcePermissions() {
        log.info("===> Iniciando processamento das Resource Permissions...");
        KeycloakService keycloakService = KeycloakService.from(url, user, password, realm);
        StringSubstitutor substitutor = new StringSubstitutor(Maps.newHashMap(Maps.fromProperties(properties)));
        ClientDefService service = new ClientDefService(keycloakService, substitutor);
        List<ResourcePermissionDef> resourcePermissionDefs = YmlService.readResourcePermissions(sourceDirectory);
        resourcePermissionDefs.forEach(service::registerRole);
    }

    private void processScopePermissions() {
        log.info("===> Iniciando processamento das Scope Permissions...");
        KeycloakService keycloakService = KeycloakService.from(url, user, password, realm);
        StringSubstitutor substitutor = new StringSubstitutor(Maps.newHashMap(Maps.fromProperties(properties)));
        ClientDefService service = new ClientDefService(keycloakService, substitutor);
        List<ScopePermissionDef> scopePermissionDefs = YmlService.readScopePermissions(sourceDirectory);
        scopePermissionDefs.forEach(service::registerRole);
    }
}

