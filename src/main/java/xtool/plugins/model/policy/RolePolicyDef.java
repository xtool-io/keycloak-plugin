package xtool.plugins.model.policy;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Getter;
import lombok.Setter;
import org.keycloak.representations.idm.authorization.Logic;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SuppressWarnings("OptionalUsedAsFieldOrParameterType")
@Getter
@Setter
@JsonTypeName("rolePolicy")
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
public class RolePolicyDef {
    private String clientId;
    private String name;
    private String description;
    private Logic logic = Logic.POSITIVE;
    private Optional<RealmDef> realm = Optional.empty();
    private Optional<ClientDef> client = Optional.empty();

    @Getter
    @Setter
    public static class ClientDef {
        private String name;
        private List<String> roles = new ArrayList<>();
    }

    @Getter
    @Setter
    public static class RealmDef {
        private List<String> roles = new ArrayList<>();
    }
}
