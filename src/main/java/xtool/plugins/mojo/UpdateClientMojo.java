package xtool.plugins.mojo;

import lombok.extern.slf4j.Slf4j;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.keycloak.admin.client.resource.ClientResource;
import org.keycloak.representations.idm.ClientRepresentation;
import xtool.plugins.service.KeycloakService;

import java.util.List;
import java.util.Objects;


@Slf4j
@Mojo(name = "update-client")
public class UpdateClientMojo extends AbstractMojo {

    /**
     * Usuário com permissão de gerenciamento de clientes no realm correspondente.
     * realm-management: [manage-clients, manage-authorization]
     */
    @Parameter(property = "keycloak.user", required = true, defaultValue = "keycloak_client_creator_user")
    private String user;

    /**
     * Senha do usuário.
     */
    @Parameter(property = "keycloak.password", required = true, defaultValue = "password")
    private String password;

    /**
     * URL de conexão do Keycloak
     */
    @Parameter(property = "keycloak.url", required = true, defaultValue = "http://localhost:8085/auth")
    private String url;

    /**
     * Nome do REALM de serviço
     */
    @Parameter(property = "keycloak.realm", required = true)
    private String realm;

    /**
     *
     */
    @Parameter(property = "keycloak.clientId", readonly = true)
    private String clientId;

    /**
     *
     */
    @Parameter
    private List<String> redirectUris;

    /**
     *
     */
    @Parameter
    private List<String> webOrigins;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        log.info("clientId: {}", clientId);
        log.info("redirectUris: {}", redirectUris);
        KeycloakService keycloakService = KeycloakService.from(url, user, password, realm);
        ClientResource clientResource = keycloakService.getClientResourceByClientId(clientId);
        ClientRepresentation representation = new ClientRepresentation();
        representation.setRedirectUris(redirectUris);
        if (Objects.nonNull(webOrigins)) representation.setWebOrigins(webOrigins);
        clientResource.update(representation);
    }
}
