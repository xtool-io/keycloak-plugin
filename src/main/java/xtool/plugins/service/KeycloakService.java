package xtool.plugins.service;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.*;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.ProtocolMapperRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.authorization.*;
import xtool.plugins.exception.ClientNotFoundException;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class KeycloakService {
    @Getter
    private final Keycloak keycloak;

    private final RealmResource realmResource;

    private KeycloakService(Keycloak keycloak, RealmResource realmResource) {
        this.keycloak = keycloak;
        this.realmResource = realmResource;
    }

    public static KeycloakService from(String url, String username, String password, String realm) {
        Keycloak kc = KeycloakBuilder
                .builder()
                .serverUrl(url)
                .realm(realm)
                .grantType(OAuth2Constants.PASSWORD)
                .clientId("admin-cli")
                .username(username)
                .password(password)
                .resteasyClient(new ResteasyClientBuilder()
                        .connectTimeout(30, TimeUnit.SECONDS)
                        .connectionPoolSize(5).build())
                .build();
        return new KeycloakService(kc, kc.realm(realm));
    }

    private Optional<ClientResource> findClientResourceByClientId(String clientId) {
        return realmResource.clients().findByClientId(clientId)
                .stream()
                .findFirst()
                .map(r -> realmResource.clients().get(r.getId()));
    }

    public ClientResource getClientResourceByClientId(String clientId) {
        return this.findClientResourceByClientId(clientId)
                .orElseThrow(() -> new ClientNotFoundException(clientId));
    }

    public List<RoleRepresentation> findAllRoles(String clientId) {
        ClientResource clientResource = this.getClientResourceByClientId(clientId);
        return clientResource.roles().list();
    }

    public void updateClient(ClientRepresentation representation) {
        List<ClientRepresentation> byClientId = realmResource.clients().findByClientId(representation.getClientId());
        if (byClientId.isEmpty()) {
            Response response = this.realmResource.clients().create(representation);
            response.close();
            return;
        }
        this.findClientResourceByClientId(representation.getClientId()).ifPresent(clientResource -> clientResource.update(representation));
    }

    public void createRole(ClientRepresentation clientRepresentation, RoleRepresentation roleRepresentation) {
        ClientResource clientResource = this.getClientResourceByClientId(clientRepresentation.getClientId());
        try {
            clientResource.roles().create(roleRepresentation);
        } catch (Exception e) {
//            throw new RuntimeException(e);
        }
    }

    public void registerCompositeRole(ClientRepresentation clientRepresentation, String role, List<String> composites) {
        ClientResource clientResource = this.getClientResourceByClientId(clientRepresentation.getClientId());
//        log.info("composites: {}", composites);
//        log.info("rolesInKyecloak: {}", rolesInKeycloak(clientResource));
        List<RoleRepresentation> roleRepresentations = composites.stream()
                .map(r -> rolesInKeycloak(clientResource).get(r))
                .collect(Collectors.toList());
        RoleResource roleResource = clientResource.roles().get(role);
        if (Objects.isNull(roleResource)) throw new NotFoundException(String.format("Role %s não encontrada", role));
        if (roleRepresentations.isEmpty()) throw new IllegalArgumentException(String.format("Lista de composite roles vazia para a role: %s", role));
//        log.info("RoleResource: {}", roleResource.toRepresentation().getName());
//        log.info("roleRepresentations: {}", roleRepresentations);
        roleResource.addComposites(roleRepresentations);
    }

    private Map<String, RoleRepresentation> rolesInKeycloak(ClientResource clientResource) {
        return clientResource.roles().list()
                .stream()
                .collect(Collectors.toMap(RoleRepresentation::getName, Function.identity()));
    }


    public void updateResource(ClientRepresentation clientRepresentation, ResourceRepresentation resourceRepresentation) {
        //log.info("updateResource(clientRepresentation={}, resourceRepresentation={})", clientRepresentation.getName(), resourceRepresentation.getName());
        ClientResource clientResource = this.getClientResourceByClientId(clientRepresentation.getClientId());
        //List<ResourceRepresentation> byName = clientResource.authorization().resources().findByName(resourceRepresentation.getName());
        Optional<ResourceRepresentation> resourceResponse = clientResource.authorization().resources().resources().stream()
                .filter(res -> res.getName().equals(resourceRepresentation.getName()))
                .findFirst();
        //log.info("clientResource.authorization().resources().findByName().isPresent()={}", resourceResponse);
        if (!resourceResponse.isPresent()) {
            clientResource.authorization().resources().create(resourceRepresentation).close();
            log.info("Recurso {} criado com sucesso.", resourceRepresentation.getName());
            return;
        }
        ResourceResource resource = clientResource.authorization().resources().resource(resourceResponse.get().getId());
        resource.update(resourceRepresentation);
        log.info("Recurso {} atualizado com sucesso.", resourceRepresentation.getName());
    }

    public void updatePolicy(ClientRepresentation clientRepresentation, RolePolicyRepresentation policyRepresentation) {
        ClientResource clientResource = this.getClientResourceByClientId(clientRepresentation.getClientId());
        RolePolicyRepresentation byName = clientResource.authorization().policies().role().findByName(policyRepresentation.getName());
        if (Objects.isNull(byName)) {
            clientResource.authorization().policies().role().create(policyRepresentation).close();
            return;
        }
        RolePolicyResource policy = clientResource.authorization().policies().role().findById(byName.getId());
        policy.update(policyRepresentation);
    }

    public void updatePolicy(ClientRepresentation clientRepresentation, JSPolicyRepresentation policyRepresentation) {
        ClientResource clientResource = this.getClientResourceByClientId(clientRepresentation.getClientId());
        JSPolicyRepresentation byName = clientResource.authorization().policies().js().findByName(policyRepresentation.getName());
        if (Objects.isNull(byName)) {
            clientResource.authorization().policies().js().create(policyRepresentation).close();
            return;
        }
        JSPolicyResource policy = clientResource.authorization().policies().js().findById(byName.getId());
        policy.update(policyRepresentation);
    }

    public void updatePermission(ClientRepresentation clientRepresentation, ResourcePermissionRepresentation resourcePermissionRepresentation) {
        ClientResource clientResource = this.getClientResourceByClientId(clientRepresentation.getClientId());
        ResourcePermissionRepresentation byName = clientResource.authorization().permissions().resource().findByName(resourcePermissionRepresentation.getName());
        if (Objects.isNull(byName)) {
            clientResource.authorization().permissions().resource().create(resourcePermissionRepresentation).close();
            return;
        }
        ResourcePermissionResource permissionResource = clientResource.authorization().permissions().resource().findById(byName.getId());
        permissionResource.update(resourcePermissionRepresentation);
    }

    public void updatePermission(ClientRepresentation clientRepresentation, ScopePermissionRepresentation scopePermissionRepresentation) {
        ClientResource clientResource = this.getClientResourceByClientId(clientRepresentation.getClientId());
        ScopePermissionRepresentation byName = clientResource.authorization().permissions().scope().findByName(scopePermissionRepresentation.getName());
        if (Objects.isNull(byName)) {
            clientResource.authorization().permissions().scope().create(scopePermissionRepresentation).close();
            return;
        }
        ScopePermissionResource permissionResource = clientResource.authorization().permissions().scope().findById(byName.getId());
        permissionResource.update(scopePermissionRepresentation);
    }

    public void updateScopeMapping(String clientId1, String clientId2, List<String> roles) {
        ClientResource clientResource1 = this.getClientResourceByClientId(clientId1);
        ClientResource clientResource2 = this.getClientResourceByClientId(clientId2);
        clientResource1.getScopeMappings().clientLevel(clientResource2.toRepresentation().getId()).add(
                roles.stream()
                        .map(role -> new RoleRepresentation(role, "", false))
                        .collect(Collectors.toList())
        );
    }

    public void updateProtocolMapper(ClientRepresentation clientRepresentation, ProtocolMapperRepresentation protocolMapperRepresentation) {
        ClientResource clientResource = this.getClientResourceByClientId(clientRepresentation.getClientId());
        Optional<ProtocolMapperRepresentation> pm = clientResource.getProtocolMappers().getMappersPerProtocol(protocolMapperRepresentation.getProtocol())
                .stream()
                .filter(protocolMapperRepresentation1 -> protocolMapperRepresentation1.getName().equals(protocolMapperRepresentation.getName()))
                .findFirst();
        if (!pm.isPresent()) {
            clientResource.getProtocolMappers().createMapper(protocolMapperRepresentation).close();
            return;
        }
        log.debug("ProtocolMapper (Id={})", pm.get().getId());
//        clientResource.getProtocolMappers().update(pm.get().getId(), protocolMapperRepresentation);
//        clientResource.getProtocolMappers().
    }

    public void removePolicy(ClientRepresentation clientRepresentation, String policyName) {
        ClientResource clientResource = this.getClientResourceByClientId(clientRepresentation.getClientId());
        clientResource.authorization().policies().policies().stream()
                .filter(p -> p.getName().equals(policyName))
                .findAny()
                .ifPresent(p -> clientResource.authorization().policies().policy(p.getId()).remove());
    }

    public void removeResource(ClientRepresentation clientRepresentation, String resourceName) {
        ClientResource clientResource = this.getClientResourceByClientId(clientRepresentation.getClientId());
        clientResource.authorization().resources().resources().stream()
                .filter(p -> p.getName().equals(resourceName))
                .findAny()
                .ifPresent(p -> clientResource.authorization().resources().resource(p.getId()).remove());
    }

}
