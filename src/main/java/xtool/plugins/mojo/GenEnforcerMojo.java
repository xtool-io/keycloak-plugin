package xtool.plugins.mojo;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import xtool.plugins.model.PolicyEnforcer;
import xtool.plugins.service.GeneratePolicyEnforcerService;
import xtool.plugins.service.YmlService;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Slf4j
@Mojo(name = "gen-enforcer")
public class GenEnforcerMojo extends AbstractMojo {


    /**
     * Parâmetro com a refência ao arquivo descritor das informações do Keycloak. (keycloak.yml)
     */
    @Parameter(property = "keycloak.descriptor", defaultValue = "src/main/resources/keycloak")
    private File sourceDirectory;

    @Parameter(defaultValue = "${project.build.directory}/classes")
    private File outputDirectory;

    @SneakyThrows
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        GeneratePolicyEnforcerService generatePolicyEnforcerService = new GeneratePolicyEnforcerService();
        List<PolicyEnforcer> policyEnforcers = YmlService.readPolicyEnforcers(sourceDirectory);
        byte[] generate = generatePolicyEnforcerService.generate(policyEnforcers);
        Files.createDirectories(Paths.get(outputDirectory.getAbsolutePath()));
        Files.write(Paths.get(outputDirectory.getAbsolutePath()).resolve("policy-enforcer.yml"), generate);
    }
}
